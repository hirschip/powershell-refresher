# Hands On Lab 2
# Author: Aurel Schwitter

# this statement prevents the script from executing by itself (F5)
# IMPORTANT: place the cursor in the line and press F8 to execute the current line!
return


<#
    Section 1: Basic Active Directory Data gathering
#>

# Task 1.1: Retrieve information about me
Get-ADUser -Identity "aurels" -Properties "telephoneNumber","l","mail"


#########################


# Task 1.2: Get all AD users that ...
#           ... reside below the OU "OU=users,OU=BIOL-MICRO,OU=BIOL,OU=Hosting,DC=d,DC=ethz,DC=ch"
#           ... Select the office & e-mail -> AD property "office" and "mail"
#
#               -> You have to provide a filter, because we want all results, we use -Filter "*" -> No filter
#
#           Hint: See 'Get-Help Get-AdUser -Examples'    -> Example 1 !
#
$Users = Get-ADUser -Filter "*" -Properties # .... complete command


# Verify your Results:
$Users | Select-Object Name, Office, Mail


#########################

# Task 1.3: There are some users without an Office! Filter these out
#           Help: 'Get-Help Where-Object -Examples' -> Example 5
$FilteredUsers = $Users |  # .... complete command

# Verify your Results:
$FilteredUsers | Select-Object Name, Office, Mail


#########################


# Task 1.4: Do some formatting:
#               - Sort by Office
#               - Show as Table with columns GivenName,Surname,Office,Mail
$FilteredUsers | Sort-Object <#Complete here#> | Format- <# Complete here #>


#########################

# Task 1.5: Did you notice? Most users are in HCI, but some are not!
#           We need to remind these users to update their office address.
#   -> Filter out all users that are not in HCI!
$NotHCI = $FilteredUsers | Where-Object  # ... complete

# Verify your Results:
$NotHCI | Select-Object Name, Office, Mail

#########################

# Task 1.6: Send these users an E-Mail to remind them
#            -> This is just an example, we're not going to send any e-mails! <-
#            -> You do not have have to write any code here. this is just an example!

foreach ($User in $NotHCI) {

    # The body of the message

    $Body = @"
Dear $($User.GivenName) $($User.Surname),

Please confirm that your Office ($($User.Office)) is still correct.
If not, please update it on http://www.adressen.ethz.ch !

Best Regards,
$env:USERNAME@ethz.ch
"@


    # Task 1.7: if you want, you can send this e-mail to yourself (to check if it's working)
    #           Uncomment the following line:
    # Send-MailMessage -To "<yourEmailHere>" -Subject "Please update your Office" -SmtpServer "smtp0.ethz.ch" -From "PS <no_reply_powershellcourse@ethz.ch>" 
    
    $Body # to output the message to the console
    Write-Host; Write-Host; Write-Host # add some spacing
}











######################## SOLUTIONS
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########

# Task 1.2
$Users = Get-ADUser -Filter "*" -Properties "mail", "office" -SearchBase "OU=users,OU=BIOL-MICRO,OU=BIOL,OU=Hosting,DC=d,DC=ethz,DC=ch"

# Task 1.3
$FilteredUsers = $Users | Where-Object Office

# Task 1.4
$FilteredUsers | Sort-Object Office | Format-Table Name, Surname, Office, Mail

# Task 1.5
$NotHCI = $FilteredUsers | Where-Object Office -NotLike "HCI*"

