return;

# 
# Operator Examples:
#
1 -eq 1         # true
1 -ne 1         # false
4 -gt 4         # false
42 -eq "World"  # false
5 - 2 -eq 3     # true

$i = 5
$i -gt 3 

#
# String Operator examples
#
“Hello World” -like “*World”
“Hello World” -match “[A-Z]*”
$Matches

“Hello”,”World” –join “_“
“Hello_World” –split “_“
“Hello Cmd!” –replace “Cmd”,”PowerShell”

#
# Array operators
#
$System32Exes1 = Get-ChildItem "C:\Windows\System32\*.exe" -Recurse 2>$null
$System32Exes1[0]
$System32Exes1 | Select-Object -First 1
$System32Exes1[-1]
$System32Exes1[-3]
$System32Exes1 | Select-Object -Last 1
$System32Exes1.Name -contains "xwizard.exe"
$System32Exes1.Name -eq "xwizard.exe"


#### PowerShell and the 'correct' way:
$System32Exes2 = Get-ChildItem "C:\Windows\System32\" -Filter "*.exe" -Recurse 2>$null
$System32Exes3 = Get-ChildItem "C:\Windows\System32\" -Recurse 2>$null | Where-Object Extension -eq ".exe"

# Same results?
Compare-Object $System32Exes1 $System32Exes2
Compare-Object $System32Exes2 $System32Exes3

# Compare time executed
Measure-Command {Get-ChildItem "C:\Windows\System32\*.exe" -Recurse 2>$null} | Select-Object TotalSeconds
Measure-Command {Get-ChildItem "C:\Windows\System32\" -Filter "*.exe" -Recurse 2>$null} | Select-Object TotalSeconds
Measure-Command {Get-ChildItem "C:\Windows\System32\" -Recurse 2>$null | Where-Object Extension -eq ".exe"} | Select-Object TotalSeconds


