set-location $PSScriptRoot
return;


# Verbs
Get-Verb

# Non normed verb example
Import-Module .\z_verbsModule.psm1
NotNormed-Function

# Help example
Get-Help Get-ChildItem

# Commands
Get-Command *-Service
Get-Command Test-*

# Modules
Get-Module 
Get-Module -ListAvailable

# Providers
Import-Module "ActiveDirectory"
Get-ChildItem "AD:\OU=Hosting,DC=D,DC=ETHZ,DC=CH"
Get-ChildItem "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate"
Get-ChildItem "Alias:\"


